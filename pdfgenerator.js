const pdfCreator = require("pdf-creator-node");
var pdf = require("html-pdf");
const fs = require('fs');

class PDFGenerator {
    constructor() {
    }

    setTemplate(templateFile) {
        this.templateFile = templateFile;
    }

    generate(report) {
        var options = { 
            format: "A4", 
            orientation: "portrait", 
            border: "10mm" ,
        };
        var template = fs.readFileSync(this.templateFile, 'utf8');
        var document = {
            html: template,
            data: report,
            
            path: './output.pdf'
        };
        // var pdfPromise = pdf.create(html, options);
        // pdfPromise.toBuffer((err, res) => {
        //     if (!err) {
        //         console.log(pdfBuffer);
        //     } else {
        //         console.log(err)
        //     }
        // })
        pdfCreator.create(document, options)
            .then(res => {
                console.log(res)
            })
            .catch(error => {
                console.error(error)
            });
    }
}
module.exports = new PDFGenerator();