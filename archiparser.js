var parseString = require('xml2js').parseString;
const fs = require('fs');

class ArchiParser {
    constructor() {
        this.report = {
            purpose: "",
            views: [],
            elements: {},
            newline: "<br/>"
        }; //nog een interface van maken
    }

    readArchi(archiFilename) {
        var xmlData = fs.readFileSync(archiFilename);
        let _self = this;
        return new Promise((resolve, reject) => {
            parseString(xmlData, function (err, result) {
                _self.report.name = result['archimate:model']['$']['name'];
                let purpose = result['archimate:model']['purpose'];
                let folders = result['archimate:model']['folder'];
                _self.report.purpose = purpose;
                _self.parseFolder(folders, 'Business');
                _self.parseFolder(folders, 'Application');
                _self.parseFolder(folders, 'Other');
                _self.parseViews(folders);
                resolve(_self.report)
            });
        })

    }

    parseViews(folders) {
        let viewsFolder = folders.find((folder) => folder['$']['name'] == 'Views');
        //console.log(JSON.stringify(viewsFolder));
        let views = viewsFolder['element'];
        let viewArray = [];
        views.forEach(view => {
            let viewName = view['$']['name'];
            let documentation = view['documentation'];
            let properties = view['property'];
            let position = 0;
            if (properties){
                properties.forEach(property => {
                    if (property['$']['key'] == 'position') {
                        position = property['$']['value'];
                    }
                })
            }
            if (position && position > 0) {
                let imgB64 = this.parseImgB64(viewName);
                let viewNode = { name: viewName, documentation: documentation, img: imgB64, position: position, elements: [] };
                viewArray.push(viewNode);
                this.parseChildren(view, viewNode);
                viewNode.elements
                    .sort((a, b) => {
                        return (a.type < b.type ? -1 : 1);
                    })
            }
        });
        viewArray
            .sort((a, b) => {
                console.log(`${a.position} > ${b.position}`);
                return parseInt(a.position) - parseInt(b.position)
            });
        this.report.views = viewArray;
        console.log(JSON.stringify(viewArray));
    }

    parseChildren(element, node) {
        let children = element['child'];
        let _self = this;

        if (children) {
            children.forEach(child => {
                let archimateId = child['$']['archimateElement'];
                if (_self.report.elements && _self.report.elements[archimateId]) {
                    let documentation = '';
                    if (_self.report.elements[archimateId].documentation) {
                        documentation = String(_self.report.elements[archimateId].documentation);
                        let regex = /\n/g;
                        documentation = documentation.replace(regex, '{{newline}}');
                    }
                    let name = _self.report.elements[archimateId].name;
                    let type = _self.report.elements[archimateId].type;
                    node.elements.push({ id: archimateId, name: name, documentation: documentation, type: type });
                }
                _self.parseChildren(child, node);
            })
        }
    }

    parseFolder(folders, layer) {
        let businessFolder = folders.find((folder) => folder['$']['name'] == layer);
        //console.log(JSON.stringify(viewsFolder));
        let folder = businessFolder['element'];
        let _self = this;
        folder.forEach(element => {
            let id = element['$']['id'];
            let documentation = element['documentation'];
            let type = element['$']['xsi:type']
            type = type.replace('archimate:', '');
            _self.report.elements[id] = { name: element['$']['name'], documentation: documentation, type: type };
        });
    }

    parseImgB64(viewName) {
        let imgFile = './resources/' + viewName + '.png';
        let image = fs.readFileSync(imgFile);
        return image.toString('base64');
    }

}
module.exports = new ArchiParser();