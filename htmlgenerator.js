const Handlebars = require("handlebars");
const fs = require('fs');

class HtmlGenerator {
    constructor() {
    }

    setTemplate(templateFile){
        this.templateFile = templateFile;
    }

    generate(data, saveHtml){
        console.log("Generating HTML, save: "+saveHtml);
        let templateString = String(fs.readFileSync(this.templateFile));
        this.template = Handlebars.compile(templateString);
        let html = this.template(data);
        let regex = /{{newline}}/g;
        let htmlWithBR = html.replace(regex, '<br/>');
        if (saveHtml){
            fs.writeFileSync('./resources/out.html', htmlWithBR);
        }
        //console.log(html);
        console.log("Generating HTML done:");
        return htmlWithBR;
    }
}
module.exports = new HtmlGenerator();