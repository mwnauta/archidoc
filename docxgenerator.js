var HtmlDocx = require('html-docx-js');
var fs = require('fs');

class DocXGenerator {
    constructor() {
    }

    generate(html) {
        console.log("Processing word document");
        var docx = HtmlDocx.asBlob(html);
        fs.writeFile('./resources/archi.docx', docx, function (err) {
            if (err) throw err;
            console.log("Word document processed....");
        });
    }
}
module.exports = new DocXGenerator();

