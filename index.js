
const htmlGenerator = require("./htmlgenerator");
const archiParser = require("./archiparser");
const docXGenerator = require("./docxgenerator");
//const pdfGenerator = require("./pdfgenerator");

htmlGenerator.setTemplate("./templates/ArchiReport.html");
//pdfGenerator.setTemplate("./resources/ArchiReport.html");
archiParser.readArchi('./resources/architectuur.archimate')
    .then(report => {
        console.log(JSON.stringify(report, null, 2));
        let html = htmlGenerator.generate(report, true);
        docXGenerator.generate(html);
        //pdfGenerator.generate(report);
    }
    );
